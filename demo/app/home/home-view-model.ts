import { Observable } from "tns-core-modules/data/observable";
import { Print } from "../../../src/";
import { knownFolders, Folder, path } from 'tns-core-modules/file-system';

export class HomeViewModel extends Observable {
    protected versionNumber: string;
    protected title: string;
    protected printer: Print;

    constructor() {
        super();
        this.printer = new Print();
        this.versionNumber = this.printer.version();
        this.title = 'Nativescript Printer Demo v. ' + this.versionNumber;
    }

    printDummyPdf() {

        const folder: Folder = knownFolders.currentApp().getFolder("app").getFolder("assets");
        const filepath: string = path.join(folder.path, "dummy.pdf");

        this.printer.printPdf(filepath).then(() => {
            console.log('Print dialog opened!');
        }).catch((error) => {
            console.error(error);
        });
    }
}
