import { Print } from 'nativescript-print';
import * as observable from "tns-core-modules/data/observable";
import { Page } from "tns-core-modules/ui/page";
import { HomeViewModel } from "./home-view-model";

export function pageLoaded(args: observable.EventData) {
    const page = <Page>args.object;

    page.bindingContext = new HomeViewModel();
}
