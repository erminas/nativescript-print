# NativeScript Print ![apple](https://cdn3.iconfinder.com/data/icons/picons-social/57/16-apple-32.png) ![android](https://cdn4.iconfinder.com/data/icons/logos-3/228/android-32.png)

![GitHub package.json version](https://img.shields.io/github/package-json/v/erminas/nativescript-print)
![GitHub package.json dependency version (prod)](https://img.shields.io/github/package-json/dependency-version/erminas/nativescript-print/nativescript-print)
![GitHub All Releases](https://img.shields.io/github/downloads/erminas/nativescript-print/nativescript-print/total)
![Twitter Follow](https://img.shields.io/twitter/follow/erminas_de?style=social)

Printer for IOS and Android.
Currently only supports *.pdf files.

## Installation

```javascript
tns plugin add nativescript-print
```

## Usage

For Angular/NativeScript:
	
```javascript
import { Print } from "nativescript-print";
import { File, knownFolders, Folder, path } from 'tns-core-modules/file-system';
.......
// init printer
const printer = new Print();

// get your filepath as string
const folder: Folder = knownFolders.currentApp().getFolder("app").getFolder("assets");
const filepath: string = path.join(folder.path, "yourfile.pdf");

// start printing dialog
printer.printPdf(filepath).then(() => {
    console.log('Print dialog opened!');
}).catch((error) => {
    console.error(error);
});
```
    
## License

Apache License Version 2.0, January 2004
