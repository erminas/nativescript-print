import { Component, OnInit } from "@angular/core";
import { Print } from "../../../../src";
import { knownFolders, Folder, path } from 'tns-core-modules/file-system';
import { EventData } from "tns-core-modules/ui/page/page";


@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    protected versionNumber: string;
    protected printer: Print;
    protected title: string;

    constructor() {
        this.printer = new Print();
    }

    printDummyPdf(args: EventData) {

        const folder: Folder = knownFolders.currentApp().getFolder("app").getFolder("assets");
        const filepath: string = path.join(folder.path, "dummy.pdf");

        this.printer.printPdf(filepath).then(() => {
            console.log('Print dialog opened!');
        }).catch((error) => {
            console.error(error);
        });
    }

    ngOnInit(): void {
        this.versionNumber = this.printer.version();
        this.title = 'Nativescript PDF Printer Demo v. ' + this.versionNumber;
    }
}
