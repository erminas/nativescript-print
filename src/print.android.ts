import * as application from "tns-core-modules/application";
import { File } from 'tns-core-modules/file-system';

declare var android: any;

export class Print {

    private context = application.android.startActivity;

    public version():string {
        const PackageManager = android.content.pm.PackageManager;
        const pkg = this.context.getPackageManager().getPackageInfo(this.context.getPackageName(),
            PackageManager.GET_META_DATA);
        return pkg.versionName;
    }

    public printPdf(path: string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            if(File.exists(path)){
                const printManager = this.context.getSystemService(android.content.Context.PRINT_SERVICE);
                try {
                    const pdfDocAdapter = new PdfDocumentAdapter(this.context, path);
                    pdfDocAdapter.onError(reject);
                    pdfDocAdapter.onSuccess(resolve);

                    const builder = new android.print.PrintAttributes.Builder();
                    printManager.print('Document', pdfDocAdapter, builder.build());
                } catch (err) {
                    reject(err);
                }
            } else {
                reject(new Error("Cant find file: "+path));
            }
        })
    }
}

class PdfDocumentAdapter extends android.print.PrintDocumentAdapter {
    private success: Function = null;
    private error: Function = null;
    private file: java.io.File = null;
    
    private callError(error: Error):boolean {
        if(this.error != null){
            if(this.error(error) === false){
                return false;
            }
        }
        return true;
    }

    private callSuccess():void {
        if(this.success != null){
            this.success();
        }
    }

    constructor(public ctxt, public pathName: string) {
        super();
        this.file = new java.io.File(pathName);
        return global.__native(this);
    }

    public onError(errorFunc: Function):void {
        this.error = errorFunc;
    }

    public onSuccess(successFunc: Function):void {
        this.success = successFunc;
    }

    public onLayout(printAttributes, printAttributes1, cancellationSignal, layoutResultCallback, bundle):void {
        if (cancellationSignal.isCanceled()) {
            layoutResultCallback.onLayoutCancelled();
        }
        else {
            const builder = new android.print.PrintDocumentInfo.Builder(this.file.getName());
            builder.setContentType(android.print.PrintDocumentInfo.CONTENT_TYPE_DOCUMENT)
                .setPageCount(android.print.PrintDocumentInfo.PAGE_COUNT_UNKNOWN)
                .build();
            layoutResultCallback.onLayoutFinished(builder.build(),
                !printAttributes1.equals(printAttributes));
        }
    }

    public onWrite(pageRanges, parcelFileDescriptor, cancellationSignal, writeResultCallback):void {
        let inStream: java.io.FileInputStream = null;
        let outStream: java.io.FileOutputStream = null;
        try {
            inStream = new java.io.FileInputStream(this.file);
            outStream = new java.io.FileOutputStream(parcelFileDescriptor.getFileDescriptor());

            const bytes = (<any>Array).create("byte", this.file.length());

            let data = inStream.read(bytes);
            if (data != -1 && !cancellationSignal.isCanceled()) {
                outStream.write(bytes, 0, bytes.length);
            }

            if (cancellationSignal.isCanceled()) {
                writeResultCallback.onWriteCancelled();
            }
            else {
                writeResultCallback.onWriteFinished([android.print.PageRange.ALL_PAGES]);
            }
        }
        catch (e) {
            if(this.callError(e) !== false){
                writeResultCallback.onWriteFailed(e+"");
            }
        }
        finally {
            try {
                inStream.close();
                outStream.close();
                this.callSuccess();
            }
            catch (e) {
                this.callError(e);
            }
        }
    }
}

