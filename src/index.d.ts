
export declare class Print {
  public version(): string;
  public printPdf(path: string);
  // define your typings manually
  // or..
  // take the ios or android .d.ts files and copy/paste them here
}
