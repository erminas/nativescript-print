import { device } from "tns-core-modules/platform";
import { DeviceType } from "tns-core-modules/ui/enums";
import * as frame from "tns-core-modules/ui/frame";
import { File } from 'tns-core-modules/file-system';

export class Print {
    version() {
        const version = NSBundle.mainBundle.objectForInfoDictionaryKey("CFBundleShortVersionString");
        return version;
    }

    private static isPrintingSupported(): boolean {
        let controller = NSClassFromString("UIPrintInteractionController");
        if (!controller) {
          return false;
        }
        return UIPrintInteractionController.sharedPrintController &&
            UIPrintInteractionController.printingAvailable;
      }

    printPdf(path: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            if (File.exists(path)) {
                if (!Print.isPrintingSupported()) {
                    reject(new Error("Printing is not supported on this device!"));
                    return;
                }

                try {
                    const controller = UIPrintInteractionController.sharedPrintController;
                    controller.showsNumberOfCopies = true;
                    controller.showsPageRange = true;

                    let printInfo = UIPrintInfo.printInfo();
                    printInfo.outputType = UIPrintInfoOutputType.General;
                    printInfo.jobName = "Print PDF";
                    controller.printInfo = printInfo;
                    controller.printingItem = NSURL.fileURLWithPath(path);

                    let callback = (controller, success, error) => {
                        resolve(success);
                    };

                    if (device.deviceType === DeviceType.Tablet) {
                        let view = UIApplication.sharedApplication.keyWindow.rootViewController.view;
                        let theFrame: any = frame.topmost().currentPage.frame;
                        controller.presentFromRectInViewAnimatedCompletionHandler(theFrame, view, true, callback);
                    } else {
                        controller.presentAnimatedCompletionHandler(true, callback);
                    }
                } catch (e) {
                    reject(e);
                }
            } else {
                reject(new Error("Cant find file: " + path));
            }
          });
    }
}
